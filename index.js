const express = require("express");
const axios = require("axios");
const bodyParser = require("body-parser");
const cors = require("cors");
const CircularJSON = require("circular-json");

// initialize the app
const app = express();

const API_TYPICODE_URL = "https://jsonplaceholder.typicode.com";
const API_IP_URL = "https://api.ipify.org?format=json";
const API_STREAMLINE_URL = "https://web.streamlinevrs.com/api/json";

const basicAuth = require("express-basic-auth");

app.use(cors());
app.use(
  basicAuth({
    users: { admin: "supersecret" },
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/ip", (req, res) => {
  axios
    .get(`${API_IP_URL}`)
    .then(data => {
      // use CircularJSON because the response
      // has circular references in it
      res.send(CircularJSON.stringify(data.data));
    })
    // check for errors
    .catch(error => {
      console.log(error.message);
    });
});

app.post("/test*", (req, res) => {
  axios
    .post(`${API_TYPICODE_URL}${req.path.substring(5)}`)
    .then(data => {
      res.send(CircularJSON.stringify(data.data));
    })
    .catch(error => {
      console.log(error.message);
    });
});

app.post("/api", (req, res) => {
  console.log(req.body);
  axios
    .post(`${API_STREAMLINE_URL}`, req.body)
    .then(data => {
      res.send(CircularJSON.stringify(data.data));
    })
    .catch(error => {
      console.log(error.message);
    });
});

app.listen(3000, () => console.log("server started"));

module.exports = {
  app,
};
