# gce-function-express-api proxy

This will establish a function in GCE to proxy API requests to any third party

## See the official documentation
### [Google Cloud Functions](https://cloud.google.com/functions)

## Overview

### GCP Cloud Functions with a Static IP

#### Synopsis

Some vendor APIs only accept requests from a whitelisted public IP.

This is the schema for a 'traditional' architecture:

![traditional architecture](./docs/images/traditional.png)

Cloud Functions will resolve most of the architecture but as you can see, 
some resources from a 'traditional' architecture are still necessary to achieve our objetive.

![serverless architecture](./docs/images/functions.png)


## Technical Details

### Overview

For security reasons, browsers restrict cross-origin HTTP requests initiated
from scripts. For example, XMLHttpRequest and the Fetch API follow the
same-origin policy. This means that a web application using those APIs can only
request resources from the same origin the application was loaded from, unless the response from other origins includes the right CORS headers.

The CORS mechanism supports secure cross-origin requests and data transfers
between browsers and servers. Modern browsers use CORS in APIs such as
XMLHttpRequest or Fetch to mitigate the risks of cross-origin HTTP requests.


To solve this issue, this function proxies requests to API endpoints
using Axios to send the request along with any headers to the API endpoint,
and subsequently forwarding those results back to the client.

Additionally BasicAuth middleware is included to prevent unauthorized access.


### Steps

#### Create a VPC

We're going to use a VPC (Virtual Private Cloud) that provides networking for cloud-based services, in this case our Cloud Function.

```
gcloud services enable compute.googleapis.com

gcloud compute networks create my-vpc \
    --subnet-mode=custom \
    --bgp-routing-mode=regional
```

#### Create a Serverless VPC Access Connector

Next we have to create a Serverless VPC Access connector that allows Cloud functions (and other Serverless resources) to connect with a VPC.

```
gcloud services enable vpcaccess.googleapis.com

gcloud compute networks vpc-access connectors create functions-connector \
    --network my-vpc \
    --region us-central1 \
    --range 10.8.0.0/28
```

#### Grant Permissions 

Before we can use our functions-connector we have to grant the appropriate permissions to the Cloud Functions service account, so the Cloud Functions will be able to connect to our functions-connector.

```
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export PROJECT_NUMBER=$(gcloud projects list --filter="$PROJECT_ID" --format="value(PROJECT_NUMBER)")

gcloud projects add-iam-policy-binding $PROJECT_ID \
--member=serviceAccount:service-$PROJECT_NUMBER@gcf-admin-robot.iam.gserviceaccount.com \
--role=roles/viewer

gcloud projects add-iam-policy-binding $PROJECT_ID \
--member=serviceAccount:service-$PROJECT_NUMBER@gcf-admin-robot.iam.gserviceaccount.com \
--role=roles/compute.networkUser
```

#### Reserve static IP

If you make a request to our Cloud Function you will see this message: 
```Error: could not handle the request```

That's because our VPC doesn't have any exit to the internet!

We fix that with the steps below. First reserve a static public IP

```
gcloud compute addresses create functions-static-ip \
    --region=us-central1

gcloud compute addresses list
# NAME                 ADDRESS/RANGE  TYPE      PURPOSE  NETWORK  REGION       SUBNET  STATUS
# functions-static-ip  34.72.171.164  EXTERNAL                    us-central1          RESERVED
```

***We have our static IP!***


#### Create the Cloud Router

Configure a Cloud Router to route our network traffic.

```
gcloud compute routers create my-router \
    --network my-vpc \
    --region us-central1
```

#### Creating Cloud Nat

This allows our instances without an external IP to send outbound packets to the internet
and receive any corresponding established inbound response packets (aka via static IP).

```
gcloud compute routers nats create my-cloud-nat-config \
    --router=my-router \
    --nat-external-ip-pool=functions-static-ip \
    --nat-all-subnet-ip-ranges \
    --enable-logging
```

#### Deploy the code!

Ok, we have the connector and the permissions, let's configure our Cloud Function to use the connector.

```
gcloud functions deploy testIP \
    --runtime python37 \
    --entry-point test_ip \
    --trigger-http \
    --allow-unauthenticated \
    --vpc-connector functions-connector \
    --egress-settings all
```


Awesome! now let's try our Cloud Functions with a new request

```
curl https://us-central1-your-project.cloudfunctions.net/testIP
{"ip": "34.72.171.164"} (our static IP!)
```

Awesome! everything is working :) a little recap:

* Created a VPC to provide networking functionalities to our Cloud Function.
* Created a Serverless VPC Access connector to allow our Cloud Function to use 
VPC functionalities (like use IPs for example).
* Granted permissions to the Cloud Functions Service Account to use network resourcing.
* Configured the Cloud Function to use the Serverless VPC Access connector and
redirect all the outbound request through the VPC
* Reserved a static IP.
* Created a Cloud Router to route our network traffic.
* Create a Cloud Nat to communicate with the outside world.
